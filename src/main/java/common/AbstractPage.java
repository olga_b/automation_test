package common;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class AbstractPage {
    private WebDriver driver;

    protected AbstractPage(final WebDriver webDriver){

        this.driver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    protected WebDriver getDriver(){
        return driver;
    }

    public void waitUntil(final ExpectedCondition condition){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(condition);

    }

}
