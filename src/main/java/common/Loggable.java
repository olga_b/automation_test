package common;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;

public interface Loggable {

    Logger log = Logger.getGlobal();

    default void setupLogging() throws IOException {
        Handler fileHandler;
        fileHandler = new FileHandler("%h/IdeaProjects/test_project/log.txt", 500000, 4, true);
        //LogManager.getLogManager().readConfiguration(new FileInputStream("logging.properies"));
        log.addHandler(fileHandler);
        log.info("Logger was configured");

    }
}
