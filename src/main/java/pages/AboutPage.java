package pages;

import common.AbstractPage;
import common.Loggable;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.io.IOException;

public class AboutPage extends AbstractPage implements Loggable{

    @FindBy(xpath = "//a[contains(@class, 'hero_home__link__desktop')]")
    private WebElement createAccountButton;

    public AboutPage(WebDriver webDriver) {
        super(webDriver);
        try {
            setupLogging();
        } catch (IOException e) {
            log.warning("Logger wasn't configured");
        }
    }

    public SignupPage clickOnCreateAccountButton(){

        waitUntil(ExpectedConditions.visibilityOf(createAccountButton));
        Actions actions = new Actions(getDriver());
        actions.click(createAccountButton).perform();
        String winHandleBefore = getDriver().getWindowHandle();
//        createAccountButton.click();
        log.info("Create Account button was clicked");
        log.info(getDriver().getWindowHandle());


        for(String winHandle : getDriver().getWindowHandles()){
            if(!winHandle.equalsIgnoreCase(winHandleBefore)){
                getDriver().switchTo().window(winHandle);
            }
        }

        return new SignupPage(getDriver());
    }

}
