package pages;

import common.AbstractPage;
import common.Loggable;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;


public class SignupPage extends AbstractPage implements Loggable {

    @FindBy(xpath = "//input[contains(@id, 'FirstName')]")
    WebElement firstName;

    @FindBy(xpath = "//input[contains(@id, 'LastName')]")
    WebElement lastName;

    @FindBy(xpath = "//input[contains(@id, 'GmailAddress')]")
    WebElement gmailAddress;

    @FindBy(xpath = "//input[contains(@id, 'Passwd')]")
    WebElement password;

    @FindBy(xpath = "//input[contains(@id, 'PasswdAgain')]")
    WebElement passwordAgain;

    @FindBy(xpath = "//input[contains(@id, 'BirthDay')]")
    WebElement birthDay;

    @FindBy(xpath = "//span[contains(@id,'BirthMonth')]/div/div")
    WebElement birthMonth;

    @FindBy(xpath = "//input[contains(@id, 'BirthYear')]")
    WebElement birthYear;

    @FindBy(xpath = "//div[contains(@id,'Gender')]")
    WebElement gender;

    @FindBy(xpath = "//input[contains(@id, 'RecoveryPhoneNumber')]")
    WebElement phoneNumber;

    @FindBy(xpath = "//input[contains(@id, 'RecoveryEmailAddress')]")
    WebElement recoveryEmail;

    @FindBy(xpath = "//input[contains(@id, 'CountryCode')]")
    WebElement countryCode;

    @FindBy(xpath = "//input[contains(@id, 'submitbutton')]")
    WebElement submitButton;

    @FindBy(xpath = "//div[contains(@id,'tos-scroll-button')]")
    WebElement scrollButton;

    @FindBy(xpath = "//input[contains(@id, 'iagreebutton')]")
    WebElement agreeButton;

    @FindBy(xpath = "//input[contains(@id,'next-button')]")
    WebElement sendCodeButton;

    @FindBy(xpath = "//input[contains(@name, 'smsUserPin')]")
    WebElement userPin;

    @FindBy(xpath = "//input[contains(@name,'VerifyPhone')]")
    WebElement verifyPhonebuton;

    public SignupPage(WebDriver webDriver) {
        super(webDriver);
        try {
            setupLogging();
        } catch (IOException e) {
            log.warning("Logger wasn't configured");
        }
    }

    public void fillRegisterForm(String fName, String lName, String address, String passwd,
                                 String day, String year, String phone,
                                String recoveryAddress){

        waitUntil(ExpectedConditions.visibilityOfAllElements(Arrays.asList(firstName,lastName,gmailAddress,
                password,passwordAgain,birthDay,birthMonth,birthYear,gender,phoneNumber,recoveryEmail)));
        firstName.sendKeys(fName);
        log.info("First Name was entered");
        lastName.sendKeys(lName);
        log.info("Last Name was entered");
        gmailAddress.sendKeys(address);
        log.info("Gmail address was entered");
        password.sendKeys(passwd);
        log.info("Password was entered");
        passwordAgain.sendKeys(passwd);
        log.info("Password was entered again");
        birthDay.sendKeys(day);
        log.info("Day of birth was entered");

        selectClicked(birthMonth);
        log.info("Month of birth was entered");

        birthYear.sendKeys(year);
        log.info("Year of birth was entered");

        selectClicked(gender);
        log.info("Gender was entered");

        phoneNumber.sendKeys(phone);
        recoveryEmail.sendKeys(recoveryAddress);

        selectClicked(countryCode);
        log.info("Country was entered");

        submitButton.click();
        log.info("Register form was submitted");

    }

    public void acceptTermsAndConditions() {
//        ((JavascriptExecutor) getDriver())
//                .executeScript("document.getElementById('tos-scroll').scrollTop = document.getElementById('tos-scroll').scrollHeight");
//        waitUntil(ExpectedConditions.elementToBeClickable(agreeButton));
//        agreeButton.click();

        waitUntil(ExpectedConditions.visibilityOf(scrollButton));

        while(scrollButton.isDisplayed()){
            log.info("scroll!");
            try{
                scrollButton.click();
            } catch (ElementNotVisibleException e){
                log.info("scrollButton is not visible !!!!!!!!!!!!!!!!!!!!!!!!!!!");
                break;
            }
        }

        waitUntil(ExpectedConditions.visibilityOf(agreeButton));

        agreeButton.click();

    }
    public void sendCode() throws InterruptedException {
        waitUntil(ExpectedConditions.visibilityOf(sendCodeButton));

        sendCodeButton.click();
        log.info("Send button was clicked.");

        while(getDriver().findElement(By.xpath("//input[contains(@name, 'smsUserPin')]")).getAttribute("value").isEmpty()){
            TimeUnit.SECONDS.sleep(1);
        }
        TimeUnit.SECONDS.sleep(15);

    }

    public void verifyPnone(){
        waitUntil(ExpectedConditions.visibilityOf(verifyPhonebuton));
        verifyPhonebuton.click();

        log.info("Phone number was verified");

        //getDriver().findElements(By.xpath("//div[contains(@class, 'welcome')]/h2[contains(text()='\" + address + \"']\""));
        getDriver().findElements(By.xpath("//div[contains(@class, 'welcome')]"));

        log.info("Account was created");

    }

    private void selectClicked(WebElement element){

        Actions builder = new Actions(getDriver());

        Action chooseMonth = builder.click(element)
                .sendKeys(Keys.ARROW_DOWN)
                .sendKeys(Keys.ENTER)
                .build();
        chooseMonth.perform();

    }

//    private void setAttribute(WebElement element, String attName, String attValue) {
//        JavascriptExecutor js = (JavascriptExecutor) getDriver();
//        js.executeScript("arguments[0].setAttribute(arguments[1], arguments[2]);",
//                element, attName, attValue);
//    }
//
//    private void setText(WebElement element, String value) {
//        JavascriptExecutor js = (JavascriptExecutor) getDriver();
//        js.executeScript("arguments[0].innerHTML = arguments[1]", element, value);
//    }


}
