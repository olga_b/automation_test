package common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;


public abstract class BaseTest {

    private WebDriver driver;

    protected BaseTest() {
        initDriver();
    }

    protected WebDriver getDriver() {
        return driver;
    }

    private void initDriver() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        //wait = new WebDriverWait(driver,10);
    }
}

