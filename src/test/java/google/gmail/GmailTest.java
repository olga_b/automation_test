package google.gmail;


import common.Loggable;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.AboutPage;
import pages.SignupPage;

import java.io.*;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;


public class GmailTest implements Loggable {

    private WebDriver driver;
    private Properties prop = new Properties();
    private File file = new File("user.properties");
    private static int i = 0;

    @BeforeTest
    @Parameters("browser")
    public void setup(String browser) throws Exception {
        if (browser.equalsIgnoreCase("firefox")) {
            System.setProperty("webdriver.gecko.driver", "geckodriver");
            driver = new FirefoxDriver();
            FirefoxProfile profile = new FirefoxProfile();
            profile.setPreference("browser.tabs.loadInBackground", "false");

        }
        else if (browser.equalsIgnoreCase("chrome")) {
            System.setProperty("webdriver.chrome.driver","chromedriver.exe");
            driver = new ChromeDriver();
        }
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        init();
    }
    public void init() {
        try {
            setupLogging();
        } catch (IOException e) {
            log.warning("Logger setup failed");
        }
        InputStreamReader fileInput = null;
        try {
            fileInput = new InputStreamReader(new FileInputStream(file), "UTF-8");
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            log.warning("Property file not found");
        }
        try {
            prop.load(fileInput);
        } catch (IOException e) {
            log.warning("Load property file failed");
        }

        driver.get(prop.getProperty("baseUrl"));
        log.info("Opening" + prop.getProperty("baseUrl"));
    }

    @Test
    public void registerGmailAccount() throws InterruptedException {
        final AboutPage aboutPage = new AboutPage(driver);
        final SignupPage signupPage = aboutPage.clickOnCreateAccountButton();
        //final SignupPage signupPage = new SignupPage(driver);
        Long dateNow = new Date().getTime();

        signupPage.fillRegisterForm(prop.getProperty("firstName"),
                prop.getProperty("lastName"),
                prop.getProperty("email")+ dateNow,
                prop.getProperty("password"),
                prop.getProperty("day"),
                prop.getProperty("year"),
                prop.getProperty("phone"),
                prop.getProperty("recoveryAddress"));

        signupPage.acceptTermsAndConditions();
        log.info("Terms and Conditions were accepted");

        signupPage.sendCode();

        signupPage.verifyPnone();

    }

    @AfterTest
    public void tearDown() {
        driver.close();
        driver.quit();
    }

}
