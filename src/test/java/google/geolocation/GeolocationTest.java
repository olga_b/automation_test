package google.geolocation;


import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class GeolocationTest {

    private File requestPath = new File("request.json");
    public final static String API_KEY = "AIzaSyCO_HhDGYf-fVY3e7s4FZPxiSBb9N2hA1I";
    public final static String BASE_URL = "https://www.googleapis.com/geolocation/v1/geolocate?key=";

    @Test
    public void testRestRequest(){

        RestTemplate restTemplate = new RestTemplate();

        String url = BASE_URL + API_KEY;
        StringBuilder request = new StringBuilder();
        try {
            Scanner scan = new Scanner(requestPath);
            scan.useDelimiter("\n");
            while(scan.hasNextLine()){
                request.append(scan.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<>(request.toString(),headers);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);

        System.out.println(response);

        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        Assert.assertTrue(response.hasBody());

        String body = response.getBody();

        Assert.assertTrue(body.contains("location"));
        Assert.assertTrue(body.contains("accuracy"));

    }
}
