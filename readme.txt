Данный проект содержит тест сьют со следующими тестами:

1. Тест регистрации на Gmail - src/test/java/google/gmail/GmailTest
   Выполняется в двух браузерах: Firefox и Chrome.
   Конфигурация данных регистрируемого пользователя находится в property файле - user.properties
2. Тест на запрос данных геолокации к Geolocation API - src/test/java/google/geolocation/GeolocationTest

Для запуска тест сьюта необходимо запустить проект из testng.xml
